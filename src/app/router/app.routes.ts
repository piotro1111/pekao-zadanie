import {Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {FeaturesComponent} from '../features/features.component';
import {AuthGuardService as AuthGuard} from '../core/auth-guard.service';
import {NotFoundComponent} from '../not-found/not-found.component';
import {HomeComponent} from '../home/home.component';
import {RedirectHomeService} from '../core/redirect-home.service';
import {CurrenciesComponent} from '../currencies/currencies.component';
import {CurrenciesResolverService} from '../core/currencies-resolver.service';

export const routes: Routes = [
  {
   path: '',
   component: HomeComponent,
   resolve: {data: RedirectHomeService},
  },
  {
    path: 'login',
    loadChildren: 'src/app/login/login.module#LoginModule',
    component: LoginComponent
  },
  {
    path: 'features',
    loadChildren: 'src/app/features/features.module#FeaturesModule',
    component: FeaturesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'currencies',
    loadChildren: 'src/app/currencies/currencies.module#CurrenciesModule',
    component: CurrenciesComponent,
    resolve: {currencies: CurrenciesResolverService},
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    loadChildren: 'src/app/not-found/not-found.module#NotFoundModule',
    component: NotFoundComponent
  }
];

