import {Component, Input} from '@angular/core';
import {CurrencyComponentSuper} from '../currency.component.super';

@Component({
  selector:    'app-currency-usd',
  templateUrl: './currency-usd.component.html',
  styles:      ['#currency-usd {background-color: darkgreen}', '.yellow-color {color: yellow}'],
})
export class CurrencyUsdComponent extends CurrencyComponentSuper {

  @Input() currency: number;

  constructor() {
    super();
  }

  revalut(currency: number): number {
    return this.format(currency / 3.00);
  }

}
