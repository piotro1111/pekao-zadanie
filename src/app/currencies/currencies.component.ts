import {Component} from '@angular/core';
import {CurrencyName} from '../models/currency.model';

@Component({
  templateUrl: './currencies.component.html',
})
export class CurrenciesComponent {

  public currencyNames: CurrencyName[] = ['BTCPLN', 'LSKPLN', 'ETHPLN'];

}

