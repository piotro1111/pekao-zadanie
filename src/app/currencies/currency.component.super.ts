import {Input} from '@angular/core';
import {CurrenciesObject, CurrencyName} from '../models/currency.model';

export class CurrencyComponentSuper {

  @Input() currencyName: CurrencyName;
  public currencies: CurrenciesObject;

  public format(currency: number): number {
    return parseFloat(currency.toFixed(2));
  }

  public formatNamePl(name: string): string {
    return `${name.substring(0, 3)}/${name.substring(3, 6)}`;
  }

  public formatNameUsd(name: string): string {
    return `${name.substring(0, 3)}/USD`;
  }

}


