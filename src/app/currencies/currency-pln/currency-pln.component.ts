import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CurrencyComponentSuper} from '../currency.component.super';

@Component({
  selector: 'app-currency-pln',
  templateUrl: './currency-pln.component.html',
  styles: ['#currency {background-color: red}', '.yellow-color {color: yellow}']
})
export class CurrencyPlnComponent extends CurrencyComponentSuper implements OnInit {

  constructor(private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.currencies = this.route.snapshot.data['currencies'];
  }


}
