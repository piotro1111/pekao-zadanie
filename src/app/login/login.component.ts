import { Component, OnInit } from '@angular/core';
import {LoginService} from './login.service';
import {LoginModel} from '../models/login.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public form: FormGroup;

  constructor(private loginService: LoginService, private fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.createForm();
  }

  public createForm(): void {
    this.form = this.fb.group({
      user: [undefined, Validators.required],
      pass: [undefined, Validators.required]
    });
  }

  public mapLoginDataform(): LoginModel {
    return {
      user: this.form.get('user').value,
      pass: this.form.get('pass').value
    };
  }

  public logUser() {
    if (this.form.valid) {
      this.loginService.updateLogin(this.mapLoginDataform());
      this.router.navigate(['/features']);
    }
  }

}
