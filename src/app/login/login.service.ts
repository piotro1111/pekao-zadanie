import {Injectable} from '@angular/core';
import {LoginModel} from '../models/login.model';
import {BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';
import {isNil} from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }


  /**
   * @TODO Przerobic interface na klasę
   */

  private readonly initialLoginState: LoginModel;

  private loginStore = new BehaviorSubject<LoginModel>(this.initialLoginState);

  public login$ = this.loginStore.asObservable().pipe(
    map(login => {
      const myInitialLoginData: LoginModel = login;

      const loginWithLocalStorage: LoginModel = {
        user: localStorage.getItem('user'),
        pass: localStorage.getItem('pass')
      };

      if (isNil(loginWithLocalStorage) || isNil(loginWithLocalStorage.user) || isNil(loginWithLocalStorage.pass)) {
        return myInitialLoginData;
      }

      return loginWithLocalStorage;

    })
  );

  public updateLogin(login: LoginModel) {
    localStorage.setItem('user', `${login.user}`);
    localStorage.setItem('pass', `${login.pass}`);
    this.loginStore.next(login);
  }

}
