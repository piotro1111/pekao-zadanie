import { Component } from '@angular/core';
import {LoginModel } from '../models/login.model';
import {isNil} from 'lodash';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  constructor(private router: Router) {
    const userLogin: LoginModel = {
      user: localStorage.getItem('user'),
      pass: localStorage.getItem('user')
    };

    if (isNil(userLogin.pass) || isNil(userLogin.user)) {
      this.router.navigate(['login']);
    } else {
      this.router.navigate(['features']);
    }
  }
}
