import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {LoginModule} from './login/login.module';
import {routes} from './router/app.routes';
import {FeaturesModule} from './features/features.module';
import {AuthService} from './core/auth.service';
import {AuthGuardService} from './core/auth-guard.service';
import {NotFoundModule} from './not-found/not-found.module';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {CurrenciesModule} from './currencies/currencies.module';
import {HttpClientModule} from '@angular/common/http';
import {CurrenciesResolverService} from './core/currencies-resolver.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    LoginModule,
    FeaturesModule,
    NotFoundModule,
    HttpClientModule,
    CurrenciesModule,
    RouterModule.forRoot(routes)
  ],
  bootstrap: [AppComponent],
  providers: [
    AuthService,
    AuthGuardService,
    CurrenciesResolverService
  ]
})
export class AppModule { }
