import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {RequestOptions} from '../models/http.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  public get<T>(uri: string, options: RequestOptions = {}, params?: HttpParams): Observable<T> {
    return this.http.get<T>('https://bitbay.net/API/Public/' + uri, {params: params, headers: {Angular: 'test'}});
  }

}
