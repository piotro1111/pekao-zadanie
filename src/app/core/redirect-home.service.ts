import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RedirectHomeService implements Resolve<void> {

  constructor(private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): void {

  }

}
