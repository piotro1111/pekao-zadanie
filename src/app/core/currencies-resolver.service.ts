import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {Resolve} from '@angular/router';
import {forkJoin} from 'rxjs';
import {map} from 'rxjs/operators';
import {CurrenciesObject, Currency} from '../models/currency.model';

@Injectable({
  providedIn: 'root'
})
export class CurrenciesResolverService implements Resolve<CurrenciesObject> {

  constructor(private api: ApiService) { }

  resolve() {
    const currencyQuery = [
      this.api.get('/BTCPLN/ticker.json'),
      this.api.get('/LSKPLN/ticker.json'),
      this.api.get('/ETHPLN/ticker.json')
    ];

    return forkJoin(currencyQuery).pipe(
      map((data: Currency[]) => {
        return {
          BTCPLN: data[0],
          LSKPLN: data[1],
          ETHPLN: data[2]
        } as CurrenciesObject;
      })
    );
  }
}
