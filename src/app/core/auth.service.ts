import { Injectable } from '@angular/core';
import {LoginModel} from '../models/login.model';
import {isNil} from 'lodash';

@Injectable()
export class AuthService {
  constructor() {
  }

  public isAuthenticated(): boolean {
    const loginWithLocalStorage: LoginModel = {
      user: localStorage.getItem('user'),
      pass: localStorage.getItem('pass')
    };
    return !isNil(loginWithLocalStorage) && !isNil(loginWithLocalStorage.user) && !isNil(loginWithLocalStorage.pass);
  }
}
