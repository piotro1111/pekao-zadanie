export declare type RequestOptions = (RequestOptionsObject | boolean);

export interface RequestOptionsObject {
  fn?: (data: any) => boolean;
  noError?: boolean;
}
