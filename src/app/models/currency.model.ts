export type CurrencyName = 'BTCPLN' | 'LSKPLN' | 'ETHPLN';

export interface Currency {
  max?: number;
  min?: number;
  last?: number;
  bid: number;
  ask?: number;
  vwap?: number;
  average?: number;
  volume?: number;
}

export interface CurrenciesObject {
  BTCPLN: Currency;
  LSKPLN: Currency;
  ETHPLN: Currency;
}
